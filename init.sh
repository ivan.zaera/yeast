function init() {
	local url="$2"
	local branch="$3"

	if [[ -e "$FLOUR_FILE" ]]
	then 
		echo "❌ Error: file $FLOUR_FILE already exists."
		exit 1
	fi

	if [[ -z "$url" ]]
	then
		header "writing $FLOUR_FILE"

		echo "\
[.install]
repos=

[.pull]
repos=

[.push]
repos=
" > "$FLOUR_FILE"
		
		exit 0
	fi

	header "git clone $url"

	git clone "$url" "$FLOUR_FILE"

	if [[ -n "$branch" ]]
	then
		cd "$FLOUR_FILE" || exit
		git checkout "$branch"
	fi
}
