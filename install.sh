function install() {
	local repos
	IFS="," read -ra repos <<< "$(iniget ".install" "repos")"

	for repo in "${repos[@]}"
	do
		cd "$PRJDIR/$repo" || exit

		header "$repo: make install"

		if [[ -f Makefile || -f makefile ]]
		then
			local saved_branch
			saved_branch="$(get_current_branch)"

			local branch
			branch="$(siniget "$repo" "install" "branch" "master")"

			git checkout -q "$branch" && make install
			echo

			git checkout -q "$saved_branch"
		else
			echo "❌ Don't know how to install project '$repo'"
			echo
		fi
	done

	cd "$PRJDIR" || exit
}
