function clone() {
	for repo in $(iniget)
	do
		header "$repo: git clone $url"

		if [ ! -d "$repo" ]
		then
			local url
			url=$(iniget "$repo" "origin")

			git clone "$url" "$repo"
		else
			echo "🦘 Skipped because repo exists"
		fi
	
		echo

		for remote in $(iniget "$repo")
		do
			if [[ "$remote" == "origin" ]]
			then 
				continue
			fi
			
			local url
			url=$(iniget "$repo" "$remote")

			(
				cd "$repo" || exit

				header "$repo: git remote add $remote $url"

				if ! git remote | grep -q "^${remote}$"
				then
					git remote add "$remote" "$url"
				else
					echo "🦘 Skipped because remote exists"
				fi

				echo
			)
		done
	done
}
