function pull() {
	local repos
	IFS="," read -ra repos <<< "$(iniget ".pull" "repos")"

	for repo in "${repos[@]}"
	do
		cd "$PRJDIR/$repo" || exit

		# Assert WC is clean or refuse instead
		if [[ -n "$(git status --porcelain)" ]]
		then
				header "$repo: git pull"
				echo "🦘 Skipping because working copy is not clean"
				echo

				continue
		fi

		local saved_branch
		saved_branch="$(get_current_branch)"

		for remote in $(iniget "$repo")
		do
			local branches
			IFS="," read -ra branches <<< "$(siniget "$repo" "pull" "remote.$remote" "master")"

			for branch in "${branches[@]}"
			do
				header "$repo: git pull $remote $branch"
				git checkout -q "$branch" && git pull "$remote" "$branch" --ff-only
			done

			echo
		done

		git checkout -q "$saved_branch"
	done

	cd "$PRJDIR" || exit
}
