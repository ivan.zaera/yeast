# yeast

A git repository manager and package installer.

## Installation

```sh
git clone https://codeberg.org/ivan.zaera/yeast.git ~/.yeast && ln -s ~/.yeast/yeast ~/.local/bin/yeast
```
