function import() {
	local cwd="$PWD"

	for dir in .* *
	do
		cd "$cwd" || exit 1

		if [[ ! -d "$dir" ]]
		then 
			continue
		fi

		if [[ -n "$(iniget "$dir")" ]]
		then
			continue
		fi

		cd "$dir" || exit 1

		if ! git status >/dev/null 2>&1
		then
			continue
		fi

		header "$dir: importing"
		echo

		echo "[$dir]" >> "$FLOUR_FILE"

		local remotes
		readarray -t remotes <<< $"$(git remote -v | grep -F "(fetch)")"

		for remote in "${remotes[@]}"
		do
			local fields
			IFS=$' \t' read -ra fields <<< "$remote"

			echo "${fields[0]}=${fields[1]}" >> "$FLOUR_FILE"
		done

		echo >> "$FLOUR_FILE"
	done

	echo
	echo 🎉 IMPORT COMPLETE: Now press a key to edit the flour.ini file

	read -r

	exec yeast edit
}
