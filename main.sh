function main() {
	case "$1" in

		clone)
			load_flour_file
			source "$MYDIR/clone.sh"
			clone
			;;

		edit)
			load_flour_file
			source "$MYDIR/edit.sh"
			edit
			;;

		import)
			load_flour_file
			source "$MYDIR/import.sh"
			import
			;;

		init)
			source "$MYDIR/init.sh"
			init "$@"
			;;

		install)
			load_flour_file
			source "$MYDIR/install.sh"
			install
			;;

		pull)
			load_flour_file
			source "$MYDIR/pull.sh"
			pull
			;;

		push)
			load_flour_file
			source "$MYDIR/push.sh"
			push
			;;

		*)
			echo "
Usage: yeast <command>

Commands:

	init		sets up a new yeast repository
	import		adds all found repos to the yeast repository
	edit		edit the yeast repository config file

	clone		clones all known repos

	pull		pulls master branch for all known repos
	push		pushes master branch for all configured repos

	install		installs all configured repos as native Linux packages


Command: init [<git repo url> [<branch>]]

	This command creates an empty flour.ini file in the current directory or, if a URL is given,
	clones the git repo linked by it to the flour.ini folder.

	The git repo must have a flour.ini inside which, in turn, is used as the reference flour.ini
	file.

	The optional branch parameter must be used when the directory must track a branch different from
	the default one. This may be useful if you want to use one single repo with a different branch
	per projects folder, server, or whatever you use to organize your projects.

	When a git repo is used, yeast will pull it from the origin before any command is run.
	
"
			;;

	esac
}
