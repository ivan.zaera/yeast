# Find FLOUR_FILE
if [[ -d "flour.ini" ]]
then
	FLOUR_FILE="$(realpath "flour.ini/flour.ini")"
	FLOUR_FILE_TRACKED=true
else
	FLOUR_FILE="$(realpath "flour.ini")"
	FLOUR_FILE_TRACKED=false
fi

# usage: assert_command <cmd>
function assert_command() {
	local cmd="$1"

	if ! type "$cmd" >/dev/null 2>&1
	then
		echo
		echo "❌ Command '$cmd' is missing: please install it in order to run yeast."
		echo

		exit 3
	fi
}

# usage: get_current_branch
function get_current_branch() {
	local branch
	branch="$(git rev-parse --abbrev-ref HEAD)"

	# Check if we are in detached state
	if [[ "$branch" == "HEAD" ]]
	then
		local gitlog
		read -ra gitlog <<< "$(git log -1 --oneline | cut -d ' ' -f 1)"

		branch="${gitlog[0]}"
	fi

	echo "$branch"
}

# usage: header <text>
function header() {
	local text="$1"

	echo "➡️ ==== $text ===="
}

# usage: iniget [<section> [key]]
# description:
# Keys starting with . never appear in listings but can be retrieved directly.
function iniget() {
	local section="$1"
	local key="$2"

	# List sections alone
	if [[ -z "$section" ]]
	then
		for section in $(grep "^\[.*\]$" "$FLOUR_FILE" | tr -d "[]")
		do
			if [[ "${section:0:1}" == "." ]]
			then
				continue;
			fi

			echo "$section"
		done

		return 0
	fi

	# List keys in a section alone
	if [[ -z "$key" ]]
	then
		for line in $FLOUR_LINES
		do
			if [[ "$line" = \[$section\]* ]] 
			then
				local keyval=${line/\[$section\]/}

				local parts
				IFS='=' read -ra parts <<< "$keyval"

				local pkey="${parts[0]}"

				if [[ "${pkey:0:1}" == "." ]]
				then
					continue;
				fi

				echo "${pkey}"
			fi
		done

		return 0
	fi

	# Find a value
	for line in $FLOUR_LINES
	do
		if [[ "$line" = \[$section\]* ]] 
		then
			local keyval=${line/\[$section\]/}

			local parts
			IFS='=' read -ra parts <<< "$keyval"

			if [[ "${parts[0]}" == "$key" ]]
			then
				echo "${parts[1]}"

				return 0
			fi
		fi
	done
}

# usage: load_flour_file
function load_flour_file() {
	if $FLOUR_FILE_TRACKED
	then
		(
			cd flour.ini || exit

			echo "➡️ ==== Updating flour.ini ===="

			if [[ -n "$(git status --porcelain)" ]]
			then
				echo "🦘 Skipping because flour.ini repo's working copy is not clean"
			else
				git pull
			fi

			echo
		)
	fi

	# Load and cache $FLOUR_FILE contents
	FLOUR_LINES=''
	if [[ -f "$FLOUR_FILE" ]]
	then
		# https://stackoverflow.com/questions/49399984/parsing-ini-file-in-bash
		# This awk line turns ini sections => [section-name]key=value
		FLOUR_LINES=$(awk '/\[/{prefix=$0; next} $1{print prefix $0}' "$FLOUR_FILE")
	fi
}

# usage:	siniget <repo> <command> <key> <default>
# example:	siniget "joshi" "push" "remote.upstream" "master"
function siniget() {
	local repo="$1"
	local cmd="$2"
	local key="$3"
	local default="$4"

	local value
	value=$(iniget ".$repo.${cmd}" "${key}")

	if [[ -z "$value" ]]
	then
		value=$(iniget ".$cmd" "${key}")
	fi

	if [[ -z "$value" ]]
	then
		value="$default"
	fi

	echo "$value"
}

