function edit() {
	cd "$(dirname "$FLOUR_FILE")" && vi "$FLOUR_FILE"

	if $FLOUR_FILE_TRACKED
	then
		cd "$(dirname "$FLOUR_FILE")" || exit
		git add "$(basename "$FLOUR_FILE")"
		git commit 

		git push
	fi
}
