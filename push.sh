function push() {
	local repos
	IFS="," read -ra repos <<< "$(iniget ".push" "repos")"

	for repo in "${repos[@]}"
	do
		cd "$PRJDIR/$repo" || exit

		for remote in $(iniget "$repo")
		do
			local branches
			IFS="," read -ra branches <<< "$(siniget "$repo" "push" "remote.$remote" "master")"

			for branch in "${branches[@]}"
			do
				header "$repo: git push $remote $branch"
				git push "$remote" "$branch"
			done

			echo
		done
	done

	cd "$PRJDIR" || exit
}
